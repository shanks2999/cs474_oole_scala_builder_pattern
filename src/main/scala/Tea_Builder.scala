sealed class BuilderState
sealed class AddedWater extends BuilderState
sealed class AddedTeaLeaves extends BuilderState
sealed class AddedSugar extends BuilderState
sealed class AddedMilk extends BuilderState

class Tea(water: String, teaLeaves: String, sugar: String, milk: String) {
  override def toString: String = water + " --> " + teaLeaves + " --> " + sugar + " --> " + milk
}

class Build_My_Tea[HasProperty <: BuilderState] (water: String, teaLeaves: String, sugar: String, milk: String)
{
  def addWater(water: String)(implicit ev: HasProperty =:= BuilderState) = new Build_My_Tea[AddedWater](water, teaLeaves, sugar, milk)
  def addTeaLeaves(teaLeaves: String)(implicit ev: HasProperty =:= AddedWater) = new Build_My_Tea[AddedTeaLeaves](water, teaLeaves, sugar, milk)
  def addSugar(sugar: String)(implicit ev: HasProperty =:= AddedTeaLeaves) = new Build_My_Tea[AddedSugar](water, teaLeaves, sugar, milk)
  def addMilk(milk: String)(implicit ev: HasProperty =:= AddedSugar) = new Build_My_Tea[AddedMilk](water, teaLeaves, sugar, milk)

  def brew(implicit ev: HasProperty =:= AddedMilk) = println(new Tea(water, teaLeaves, sugar, milk))
}

object WATER extends Enumeration {  val Mineral, Spring, Prepared = Value }
object LEAVES extends Enumeration { val Green, White, Herbal, Oolong = Value  }
object SUGAR extends Enumeration {  val Cane, Caster, Sanding, Brown = Value  }
object MILK extends Enumeration { val Raw, Whole, Skim, Chocolate = Value }

object Tea_Builder {
  def main(args: Array[String]): Unit = {
    val teaPreparation = new Build_My_Tea[BuilderState]("", "", "", "")
    val tea = teaPreparation
      .addWater(WATER.Mineral.toString)
      .addTeaLeaves(LEAVES.Oolong.toString)
      .addSugar(SUGAR.Brown.toString)
      .addMilk(MILK.Skim.toString).brew
    print(tea.toString())
  }
}