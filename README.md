# README #

### This is the Builder Pattern I made for TEA preparation in scala ###

* The class is Tea_Builder.scala present in src>main>scala
* It is being us to construct an instance of Tea Class.
* The available of methods in the builder are : addWater, addTeaLeaves, addSugar and addMilk
* The brew() method is responsible for creating an instance of the tea class.
* The enforced order of functions is -> addWater then addTeaLeaves then addSugar then addMilk.
* Link which I used for reference :

	http://blog.rafaelferreira.net/2008/07/type-safe-builder-pattern-in-scala.html
	
	http://www.tikalk.com/java/type-safe-builder-scala-using-type-constraints/

Thanks
Shashank